import static org.mockito.Mockito.mock;

import java.util.function.Consumer;

import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;

import com.citi.trading.Investor;
import com.citi.trading.Market;
import com.citi.trading.OrderPlacer;
import com.citi.trading.Trade;

public class InvestorTest {
	private Investor investor;
	public static class MockMarket implements OrderPlacer{
		
		@Override
		public void placeOrder(Trade order, Consumer<Trade> callback) {
			callback.accept(order);
		}
	}
	@Before
	public void setUp() {
		investor = new Investor(10000);
		
	}
	@Test
	public void testBuy() {
		Market market = mock(Market.class);
		Mokito.doAnswer(new Answer<Object>() {
			@Override
			public Object answer(InvocationOnMock a) throws Throwable {
				Trade order = a.getArgument(0);
				Consumer<Trade> c = a.getArgument(1);
				c.accept(order);
				return null;
			}
		}).when(market).placeOrder(any(Trace.class), any(Consumer.class));;
		investor.setMarket(market);
		investor.buy("MRK", 100, 60);
		assertThat(investor.getPortfolio(),hasKey("MRK"));
		assertThat(investor.getCash(), equalTo(4000.0));
		//assertEqual(investor.cash, 4000);
		//assertEqual();
		
		
	}

}
